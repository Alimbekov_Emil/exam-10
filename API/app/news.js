const path = require("path");
const express = require("express");
const mysqlDb = require("../mysqlDb");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});
const router = express.Router();

const upload = multer({ storage });

router.get("/", async (req, res) => {
  try {
    const [news] = await mysqlDb.getConnection().query("SELECT id,title,image,datetime FROM news");
    return res.send(news);
  } catch (error) {
    return res.status(400).send({ error });
  }
});

router.get("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDb.getConnection().query("SELECT * FROM news WHERE id = ?", [req.params.id]);

    const news = result[0];

    if (news === undefined) {
      return res.send("There is no such news");
    }

    return res.send(news);
  } catch (error) {
    return res.status(400).send({ error });
  }
});

router.post("/", upload.single("image"), async (req, res) => {
  const news = req.body;
  try {
    if (news.title && news.description) {
      if (req.file) {
        news.image = req.file.filename;
      }

      news.datetime = new Date().toISOString();

      const [result] = await mysqlDb.getConnection()
        .query("INSERT INTO news (title,description,image,datetime) VALUES (?,?,?,?)", [
          news.title,
          news.description,
          news.image,
          news.datetime,
        ]);

      return res.send({ ...news, id: result.insertId });
    }
  } catch (error) {
    return res.status(400).send({ error });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    await mysqlDb.getConnection().query("DELETE FROM news WHERE id = ?", [req.params.id]);
    return res.send("Removal was successful");
  } catch (error) {
    return res.status(400).send({ error });
  }
});

module.exports = router;
