const express = require("express");
const mysqlDb = require("../mysqlDb");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    if (req.query.news_id) {
      const [comments_id] = await mysqlDb
        .getConnection()
        .query("SELECT * FROM comments WHERE news_id = ?", [req.query.news_id]);
      return res.send(comments_id);
    }
    const [comments] = await mysqlDb.getConnection().query("SELECT * FROM comments");
    return res.send(comments);
  } catch (error) {
    return res.status(400).send({ error });
  }
});

router.post("/", async (req, res) => {
  const comment = req.body;
  try {
    if (comment.comment) {
      if (comment.author === "") {
        comment.author = "Anonymous";
      }

      comment.datetime = new Date().toISOString();

      const [result] = await mysqlDb.getConnection()
        .query("INSERT INTO comments (news_id,author,comment,datetime) VALUES (?,?,?,?)", [
          comment.news_id,
          comment.author,
          comment.comment,
          comment.datetime,
        ]);

      return res.send({ ...comment, id: result.insertId });
    }
  } catch (error) {
    return res.status(400).send({ error });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    await mysqlDb.getConnection().query("DELETE FROM comments WHERE id = ?", [req.params.id]);
    return res.send("Removal was successful");
  } catch (error) {
    return res.status(400).send({ error });
  }
});

module.exports = router;
