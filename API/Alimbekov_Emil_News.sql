create schema Alimbekov_Emil_News collate utf8_general_ci;

use Alimbekov_Emil_News;

create table news
(
	id int auto_increment,
	title varchar(255) not null,
	description text not null,
	image varchar(45) null,
	datetime text null,
	constraint news_pk
		primary key (id)
);

create table comments
(
	id int auto_increment,
	news_id int not null,
	author varchar(255) default 'Anonymous' null,
	comment text not null,
	datetime text null,
	constraint comments_pk
		primary key (id),
	constraint comments_news_id_fk
		foreign key (news_id) references news (id)
			on update cascade on delete cascade
);


INSERT INTO news (id, title, description,datetime)
    VALUES
    (1, 'Article1', 'Some description1','2021-03-27T10:21:11.277Z'),
    (2, 'Article2', 'Some description2','2021-03-27T10:21:11.277Z'),
    (3, 'Article3', 'Some description3','2021-03-27T10:21:11.277Z');


INSERT INTO comments (id,news_id,author,comment,datetime)
    VALUES
    (1, 1,'Anonymus', 'Comment1','2021-03-27T10:21:11.277Z'),
    (2, 2,'Emil', 'Comment2','2021-03-27T10:21:11.277Z'),
    (3, 3,'Emil1', 'Comment3','2021-03-27T10:21:11.277Z');
