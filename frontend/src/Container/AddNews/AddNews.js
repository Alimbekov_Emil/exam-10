import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FileInput from "./FileInput";
import { useDispatch } from "react-redux";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import { postNews } from "../../store/actions/NewsActions";

const AddNews = ({ history }) => {
  const dispatch = useDispatch();

  const [news, setNews] = useState({
    title: "",
    description: "",
    image: "",
  });

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setNews((PrevState) => ({
      ...PrevState,
      [name]: value,
    }));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setNews((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const submitForm = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(news).forEach((key) => {
      formData.append(key, news[key]);
    });

    dispatch(postNews(formData));

    setNews({
      title: "",
      description: "",
      image: "",
    });
    history.push("/");
  };

  return (
    <form onSubmit={submitForm} style={{ marginTop: "50px" }}>
      <Typography variant="h4">ADD News</Typography>
      <Grid container direction="column" spacing={2}>
        <Grid item xs>
          <TextField
            fullWidth
            variant="outlined"
            label="Title"
            name="title"
            value={news.title}
            onChange={inputChangeHandler}
            required
          />
        </Grid>
        <Grid item xs>
          <TextField
            multiline
            fullWidth
            rows={5}
            variant="outlined"
            label="Content"
            name="description"
            value={news.description}
            onChange={inputChangeHandler}
            required
          />
        </Grid>
        <Grid item xs>
          <FileInput name="image" label="Image" onChange={fileChangeHandler} />
        </Grid>
        <Grid item xs>
          <Button type="submit" color="primary" variant="contained" style={{ margin: "10px" }}>
            ADD
          </Button>
          <Button
            type="button"
            color="primary"
            variant="contained"
            style={{ margin: "10px" }}
            component={Link}
            to="/"
          >
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNews;
