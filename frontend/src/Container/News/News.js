import { Button, LinearProgress, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Post from "../../Component/Post/Post";
import { fetchNews } from "../../store/actions/NewsActions";

const News = () => {
  const dispatch = useDispatch();

  const news = useSelector((state) => state.news.news);
  const loading = useSelector((state) => state.news.loading);

  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  return (
    <Grid container direction="column" spacing={2} style={{ marginTop: "20px", border: "3px solid black" }}>
      {loading ? <LinearProgress /> : null}
      <Grid item container justify="space-around" alignItems="center">
        <Grid item>
          <Typography variant="h4">News</Typography>
        </Grid>
        <Grid item>
          <Button color="primary" variant="contained" component={Link} to="/add-news">
            Add News
          </Button>
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        {news.map((post) => {
          return (
            <Post key={post.id} id={post.id} title={post.title} image={post.image} datetime={post.datetime} />
          );
        })}
      </Grid>
    </Grid>
  );
};

export default News;
