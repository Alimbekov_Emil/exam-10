import axiosNews from "../../axios-news";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const POST_COMMENTS_REQUEST = "POST_COMMENTS_REQUEST";
export const POST_COMMENTS_SUCCESS = "POST_COMMENTS_SUCCESS";
export const POST_COMMENTS_FAILURE = "POST_COMMENTS_FAILURE";

export const DELETE_COMMENTS_REQUEST = "DELETE_COMMENTS_REQUEST";
export const DELETE_COMMENTS_SUCCESS = "DELETE_COMMENTS_SUCCESS";
export const DELETE_COMMENTS_FAILURE = "DELETE_COMMENTS_FAILURE";

export const fetchCommentsRequest = () => ({ type: FETCH_COMMENTS_REQUEST });
export const fetchCommentsSuccess = (comments) => ({ type: FETCH_COMMENTS_SUCCESS, comments });
export const fetchCommentsFailure = (error) => ({ type: FETCH_COMMENTS_FAILURE, error });

export const postCommentsRequest = () => ({ type: POST_COMMENTS_REQUEST });
export const postCommentsSuccess = () => ({ type: POST_COMMENTS_SUCCESS });
export const postCommentsFailure = (error) => ({ type: POST_COMMENTS_FAILURE, error });

export const deleteCommentsRequest = () => ({ type: DELETE_COMMENTS_REQUEST });
export const deleteCommentsSuccess = () => ({ type: DELETE_COMMENTS_SUCCESS });
export const deleteCommentsFailure = (error) => ({ type: DELETE_COMMENTS_FAILURE, error });

export const fetchComments = (id) => {
  return async (dispatch) => {
    dispatch(fetchCommentsRequest());
    try {
      const response = await axiosNews.get("/comments?news_id=" + id);
      dispatch(fetchCommentsSuccess(response.data));
    } catch (e) {
      dispatch(fetchCommentsFailure(e));
    }
  };
};

export const postComments = (comment, id) => {
  return async (dispatch) => {
    dispatch(postCommentsRequest());
    try {
      await axiosNews.post("/comments", comment);
      dispatch(postCommentsSuccess());
      dispatch(fetchComments(id));
    } catch (e) {
      dispatch(postCommentsFailure(e));
    }
  };
};

export const deleteComments = (id, news_id) => {
  return async (dispatch) => {
    dispatch(deleteCommentsRequest());
    try {
      await axiosNews.delete("/comments/" + id);
      dispatch(deleteCommentsSuccess());
      dispatch(fetchComments(news_id));
    } catch (e) {
      dispatch(deleteCommentsFailure(e));
    }
  };
};
