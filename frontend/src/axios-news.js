import axios from "axios";
import { apiURL } from "./config";

const axiosNews = axios.create({
  baseURL: apiURL,
});

export default axiosNews;
