import { Route, Switch } from "react-router-dom";
import Container from "@material-ui/core/Container";
import News from "./Container/News/News";
import AddNews from "./Container/AddNews/AddNews";
import FullPost from "./Component/FullPost/FullPost";

const App = () => (
  <Container maxWidth="xl">
    <Switch>
      <Route path="/" exact component={News} />
      <Route path="/add-news" exact component={AddNews} />
      <Route path="/news/:id" component={FullPost} />
    </Switch>
  </Container>
);

export default App;
